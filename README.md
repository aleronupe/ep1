Conway's Game of Life
------------------------    
20/09/2017  
Alexandre Miguel Rodrigues Nunes Pereira  
16/0000840  
aleronupe@hotmail.com

O Jogo
----------------------
- Essa versão do jogo da vida permite a inserção de formas pré definidas e a execução de loops convencionais como "Gosper Glider Gun" e "Infinite Growth"

O Jogo segue um número pré definido de regras, acerca de uma célula (que pode estar viva ou morta) que é representada por um elemento na matriz, e suas 8 células vizinhas, que são representadas pelos elementos adjacentes. Assim, as regras são tais que:

1. Qualquer célula viva com menos de dois vizinhos vivos morre de solidão.
2. Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação.
3. Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva.
4. Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração.


Uso
------------------------
Para dar início ao jogo, é necessário estar na pasta "ep1" e utilizar o comando "make" para a criação dos arquivos
Em seguida, caso não seja apresentada nenhuma mensagem de erro, o comando "make run" será responsável por inicializar o jogo.

- Na tela inicial, é exibida uma mensagem de boas vindas, bem como a impressão do tabuleiro do jogo, porém vazio.
- O Tabuleiro é uma matriz, composta por 30 Linhas e 66 Colunas, em que cada elemento corresponde a uma célula em algum estado (viva ou morta).
- No Tabuleiro, o estágio das células é definido como:
" - " Para Células mortas
" 0 " Para Células vivas
- Nessa tela, espera-se a entrada do usuário com o modo de jogo que ele deseja.
Os modos de jogo estão expostos abaixo do Tabuleiro, com um inteiro associado a cada um desses modos.
- Caso o usuário insira o número 1, o modo criativo será ativado e a tela passará a exibir o menu para esse modo.
- Caso o usuário insira o número 2, o modo padrão será ativado e a tela passará a exibir o menu para esse modo.
- Caso algum número diferente de 1 ou 2 seja inserido, o programa apresentará uma mensagem de erro "ERRO! #1" e a construção do Tabuleiro indicará que a execução foi encerrada e somente uma nova entrada "make run" no terminal do Linux irá reiniciar o Jogo.
O Usuário não pode acessar os dois modos de jogo em uma mesma sessão.


1 - Modo Criativo
------------------------
- Nesse modo é exibido o menu contendo 8 formas de organização de células para serem inseridas.
- Cada Célula é construída de maneira independente e partindo de ponto cruciais para sua figura.

  1. Célula Comum - Célula unitária, corresponde a uma única linha e coluna da matriz estando viva;

  2. Block - Quatro células vivas formando um quadrado, construída a partir do quadrado superior direito;

  3. Blinker - Três Células oscilantes, construída a partir da célula central;

  4. Glider - Conjunto de Cinco células, 4 delas formando um L invertido horizonetalmente e a Quinta disposta em digonal à ponta do L, sendo a célula central por sua construção é feita;

  5. Fish Queen Bee - Conjunto de células unitárias formando um ciclo que se movimenta pelo tabuleiro, sendo construída a partir da célula no centro da forma;

  6. Spaceship Queen Bee - Conjunto de Células que se assemelham a uma nave espacial, sendo construída a partir da céulula no canto superior direito;

  7. Die Hard - Conjunto de quatro Células, 3 delas dispostas horizontalmente e uma, centralmente, duas linhas acima, por onde é construída;

  8. Python - Nomeada assim por se assemelhar a uma cobra, é um conjunto estático de 7 células, uma central, 3 dispostas à esquerda formando um canto inferior e 3 dipostas à direita formando um canto superior, conectados pela célula central, por onde é construído.

- Caso o valor inserido difira dos expostos como as opções, será mostrada uma tela de erro acusando o ERRO! #2, o programa será terminado e, para que se acesse de novo o jogo, é necessária inserção de outro comando "make run"
- Ao inserir o tipo de célula, o programa requisitará as coordenadas de construção, um inteiro para a linha e um inteiro para a coluna, respectivamente
- Para cada tipo de célula estarão expostos os limites máximos e mínimos para construção de cada uma das formas
- O Programa não apresentará mensagens de erro caso o limite seja excedido, estando esse cuidado a cargo do usuário
- Desrespeito aos limites estabelecidos IRÁ OCASIONAR ERROS
- Após inserir a posição, a forma aparecerá no tabuleiro, conforme a posição indicada e o programa irá perguntar se o usuário deseja inserir alguma outra célula do mesmo tipo
- Caso o usuário digite uma resposta diferente das listadas pelo programa (S ou N), essa resposta será entendida como N
- Caso o usuário não opte por uma nova inserção de células, o programa irá pedir por um inteiro explicitando o número de gerações com que se deseja rodar o jogo
- Após a inserção do inteiro X, o programa apresentará as X gerações conforme as regras do Jogo da Vida de Conway


2 - Modo Padrão
------------------------
- Nesse modo é exibido o menu contendo 2 padrões pré estabelecidos.
(1) Gosper Glider Gun - Conjunto de 2 Blocks, 1 Fish Queen Bee e 1 Spaceship Queen Bee dispostos em posições que permitem a geração de um glider, ocorrendo em um loop infinito;
(2) Infinite Growth - Conjunto de 13 células dispostas de forma a formar um tipo de quadrado e a se espalhar pelo tabuleiro por, pelo menos, 200 gerações;
- Caso o valor inserido difira dos expostos como as opções (1 e 2) e 15, será mostrada uma tela de erro acusando o ERRO! #1, o programa será terminado e, para que se acesse de novo o jogo, é necessária inserção de outro comando "make run"
- Caso o valor escolhido seja válido, o programa irá pedir por um inteiro explicitando o número de gerações com que se deseja rodar o jogo
- Após a inserção do inteiro X, o programa apresentará as X gerações conforme as regras do Jogo da Vida de Conway

Caso se deseje interromper a execução antes do fim das gerações estabelecidas, é necessário utilizar o comando "ctrl + c"
