#ifndef FISH_HPP
#define FISH_HPP

#include "celula.hpp"
#include <string>

using namespace std;

class Fish : public Celula{
	private:
		string name5;
	public:
		Fish();
		~Fish();

		//sobrescrita de método
		void setIndicador(int linha, int coluna);
		bool** getIndicador();

};
#endif
