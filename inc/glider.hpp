#ifndef GLIDER_HPP
#define GLIDER_HPP

#include "celula.hpp"
#include <string>

using namespace std;

class Glider : public Celula{
	private:
		string name2;
	public:
		Glider();
		~Glider();

		void setIndicador(int linha, int coluna);
		bool** getIndicador();

};
#endif
