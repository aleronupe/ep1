#ifndef BLOCK_HPP
#define BLOCK_HPP

#include "celula.hpp"
#include <string>

using namespace std;

class Block : public Celula{
	private:
		string name1;
	public:
		Block();
		~Block();

		//sobrescrita de método
		void setIndicador(int linha, int coluna);
		bool** getIndicador();

};
#endif
