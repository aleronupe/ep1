#ifndef DIEHARD_HPP
#define DIEHARD_HPP

#include "celula.hpp"
#include <string>

using namespace std;

class DieHard : public Celula{
	private:
		string name7;
	public:
		DieHard();
		~DieHard();

		//sobrescrita de método
		void setIndicador(int linha, int coluna);
		bool** getIndicador();

};
#endif
