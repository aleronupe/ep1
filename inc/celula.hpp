#ifndef CELULA_HPP
#define CELULA_HPP

#include <string>

using namespace std;

    class Celula{

	private:

		int linha;  //posição = [linha][coluna]
		int coluna;
		bool status; //true = viva | false = morta
    string tipo;

  protected:

    bool **indicador;

	public:

		Celula();
		~Celula();

		int getLinha();
    int getColuna();
		void setCord();

    bool getStat();
    void setStat(bool status);

		string getTipo();
		void setTipo(string tipo);

    bool** getIndicador();
    void setIndicador(int linhas, int colunas);
    void apagador();
};

#endif
