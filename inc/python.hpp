#ifndef PYTHON_HPP
#define PYTHON_HPP

#include "celula.hpp"
#include <string>

using namespace std;

class Python : public Celula{
	private:
		string name8;
	public:
		Python();
		~Python();

		//sobrescrita de método
		void setIndicador(int linha, int coluna);
		bool** getIndicador();

};
#endif
