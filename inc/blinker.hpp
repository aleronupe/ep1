#ifndef BLINKER_HPP
#define BLINKER_HPP

#include "celula.hpp"
#include <string>

using namespace std;

class Blinker : public Celula{
	private:
		string name3;
	public:
		Blinker();
		~Blinker();

		void setIndicador(int linha, int coluna);
		bool** getIndicador();

};
#endif
