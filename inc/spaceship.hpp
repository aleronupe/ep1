#ifndef SPACESHIP_HPP
#define SPACESHIP_HPP

#include "celula.hpp"
#include <string>

using namespace std;

class Spaceship : public Celula{
  private:
    string name6;

  public:
    Spaceship();
    ~Spaceship();

    //sobrescrita de métodos
    void setIndicador(int linha, int coluna);
    bool** getIndicador();


};

#endif
