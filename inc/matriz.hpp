#ifndef MATRIZ_HPP
#define MATRIZ_HPP


#include<string>

using namespace std;

class Matriz{
	private:
		bool campo[100][100];
		bool trans[100][100];
		char canvas[100][100];

	public:
		Matriz();
		~Matriz();

		bool getCampo(int i, int j);
		void setCampo(bool** indicador);

		void Executor(int cont1);
		void Desenhar();
		void AplicaRegra();

};

#endif
