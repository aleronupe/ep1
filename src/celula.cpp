#include <iostream>
#include "celula.hpp"
#include "matriz.hpp"

using namespace std;

Celula::Celula(){
	linha = 0;
	coluna = 0;
	status = true;
	tipo = "Comum";
	indicador = new bool* [100];
		for(int i = 0; i < 100; i++){
			indicador[i] = new bool[100];
				for(int j = 0; j < 100; j++){
					indicador[i][j] = false;
				}
		}
}

Celula::~Celula(){
// cout << " " << endl;

}

int Celula::getLinha(){
				return linha;
				}

int Celula::getColuna(){
				return coluna;
				}

void Celula::setCord(){
	/*this->linha = linha;
	this->coluna = coluna;*/
	cin >> linha;
	cin >> coluna;
	}

bool Celula::getStat(){
        return status;
        }
void Celula::setStat(bool status){
        this->status = status;
        }

string Celula::getTipo(){
        return tipo;
        }
void Celula::setTipo(string tipo){
        this->tipo = tipo;
        }

bool** Celula::getIndicador(){
				return indicador;
				}

void Celula::setIndicador(int linha, int coluna){
				this->indicador[linha][coluna] = true;
				}

void Celula::apagador(){
	for(int i = 0; i < 100; ++i){
		delete [] indicador[i];
	}
delete [] indicador;
}
