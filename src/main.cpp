#include<iostream>
#include "matriz.hpp"
#include "celula.hpp"
#include "block.hpp"
#include "blinker.hpp"
#include "glider.hpp"
#include "spaceship.hpp"
#include "fish.hpp"
#include "diehard.hpp"
#include "python.hpp"


using namespace std;

int main(int argc, char ** argv){

	Matriz tent1;

	cout << "\nBem vindo ao jogo da Vida\n" << endl;
	tent1.Desenhar();
	cout << "\n\nEscolha o modo de Jogo:\n" << endl;
	cout << "1 - Modo Criativo" << endl;
	cout << "2 - Modo Padrão\n\n\n\n" << endl;
	int decide1;
	cin  >> decide1;

	if(decide1 == 1){
			char decide3 = 'S';
			bool decide4 = 1;

			while(decide3 != 'N' && decide3 != 'n'){
					cout << "\nModo Criativo\n" << endl;
					tent1.Desenhar();
					cout << "\n\n";
					cout << "Escolha o tipo de forma que deseja colocar:" << endl;
					cout << "\n";
					cout << "1 - Célula Comum		5 - Fish Queen Bee" << endl;
					cout << "2 - Block			6 - Spaceship Queen Bee" << endl;
					cout << "3 - Blinker			7 - Die Hard" << endl;
					cout << "4 - Glider			8 - Python" << endl;
					cout << "\n\n";
					cin  >> decide1;

					if(decide1 == 1){
							Celula prim1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "0 <= Linha <= 29" << endl;
								cout << "0 <= Coluna <= 65\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								prim1.setCord();
								prim1.setIndicador(prim1.getLinha(), prim1.getColuna());
								tent1.setCampo(prim1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outra célula comum?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}

						}
								prim1.apagador();
					}

					else if(decide1 == 2){
							Block bloco1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "0 <= Linha <= 28" << endl;
								cout << "0 <= Coluna <= 64\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								bloco1.setCord();
								bloco1.setIndicador(bloco1.getLinha(), bloco1.getColuna());
								tent1.setCampo(bloco1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outro Block?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}
							}
								bloco1.apagador();
					}

					else if(decide1 == 3){
							Blinker blink1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "1 <= Linha <= 28" << endl;
								cout << "0 <= Coluna <= 65\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								blink1.setCord();
								blink1.setIndicador(blink1.getLinha(), blink1.getColuna());
								tent1.setCampo(blink1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outro Blinker?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}
							}
								blink1.apagador();
					}

					else if(decide1 == 4){
							Glider glide1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "1 <= Linha <= 28" << endl;
								cout << "0 <= Coluna <= 63\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								glide1.setCord();
								glide1.setIndicador(glide1.getLinha(), glide1.getColuna());
								tent1.setCampo(glide1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outro Glider?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}
							}
								glide1.apagador();
					}

					else if(decide1 == 5){
							Fish fish1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "3 <= Linha <= 26" << endl;
								cout << "4 <= Coluna <= 62\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								fish1.setCord();
								fish1.setIndicador(fish1.getLinha(), fish1.getColuna());
								tent1.setCampo(fish1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outra Fish Queen Bee?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}
						}
								fish1.apagador();
					}

					else if(decide1 == 6){
							Spaceship ship1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "2 <= Linha <= 25" << endl;
								cout << "0 <= Coluna <= 61\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								ship1.setCord();
								ship1.setIndicador(ship1.getLinha(), ship1.getColuna());
								tent1.setCampo(ship1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outra Spaceship Queen Bee?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}
						}
								ship1.apagador();
					}

					else if(decide1 == 7){
							DieHard diehard1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "0 <= Linha <= 27" << endl;
								cout << "1 <= Coluna <= 64\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								diehard1.setCord();
								diehard1.setIndicador(diehard1.getLinha(), diehard1.getColuna());
								tent1.setCampo(diehard1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outro Die Hard?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}
							}
								diehard1.apagador();
					}

					else if(decide1 == 8){
							Python snake1;
							char decide2 = 'S';

							while(decide2 != 'N' && decide2 != 'n'){
								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\n";
								cout << "Insira a posição central (Linha e Coluna):" << endl;
								cout << "1 <= Linha <= 28" << endl;
								cout << "2 <= Coluna <= 63\n\n\n\n\n" << endl;
								cout << "Entrada: ";
								snake1.setCord();
								snake1.setIndicador(snake1.getLinha(), snake1.getColuna());
								tent1.setCampo(snake1.getIndicador());

								cout << "\nModo Criativo\n" << endl;
								tent1.Desenhar();
								cout << "\n\nInserir outra Python?" << endl;
								cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
								cin  >> decide2;
								if(decide2 != 'S' && decide2 != 's'){
									decide2 = 'N';
								}
							}
								snake1.apagador();
					}

					else{
						Celula prim1;
						for(int i = 0; i < 30 ; i++){
							for(int j = 0; j < 66 ; j++){
								if (i%2 != 0 && j%2 == 0) {
									prim1.setIndicador(i , j);
									tent1.setCampo(prim1.getIndicador());
								}
								else if (i%2 == 0 && j%2 != 0) {
									prim1.setIndicador(i , j);
									tent1.setCampo(prim1.getIndicador());
								}
							}
						}
						prim1.apagador();
						cout << "\nJogo da Vida\n" << endl;
						tent1.Desenhar();
						cout << "\n\nERROR! #02" << endl;
						cout << "Opção Inválida\n\n\n\n\n\n" << endl;
						decide4 = 0;
						break;
					}

					cout << "\nModo Criativo\n" << endl;
					tent1.Desenhar();
					cout << "\n\nDeseja colocar mais alguma célula?" << endl;
					cout << "S - Sim | N - Não\n\n\n\n\n\n" << endl;
					cin >> decide3;
					if(decide3 != 'S' && decide3 != 's'){
						decide3 = 'N';
					}
				}

			if(decide4 == 1){
				cout << "\nJogo da Vida\n" << endl;
				tent1.Desenhar();
				cout << "\n\nCom Quantas gerações deseja executar o jogo?" << endl;
				cout << "\n\n\n\n\n\n" << endl;
				cin >> decide1;
				tent1.Executor(decide1);
			}
	}

else if(decide1 == 2){

	bool decide4 = 1;
	cout << "\nModo Padrão\n" << endl;
	tent1.Desenhar();
	cout << "\n\n";
	cout << "Escolha o padrão de formas que deseja colocar:" << endl;
	cout << "\n";
	cout << "1 - Gosper Glider Gun" << endl;
	cout << "2 - Infinite Growth" << endl;
	cout << "" << endl;
	cout << "" << endl;
	cout << "\n\n";
	cin  >> decide1;

	if(decide1 == 2){
		Celula prim1;
		prim1.setIndicador(10 , 32);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(10 , 33);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(10 , 34);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(10 , 36);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(11 , 32);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(12 , 35);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(12 , 36);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(13 , 33);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(13 , 34);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(13 , 36);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(14 , 32);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(14 , 34);
		tent1.setCampo(prim1.getIndicador());
		prim1.setIndicador(14 , 36);
		tent1.setCampo(prim1.getIndicador());
		prim1.apagador();
	}

	else if(decide1 == 1){
		Fish fish1;
		Spaceship ship1;
		Block bloco1;

		bloco1.setIndicador(12 , 5);
		tent1.setCampo(bloco1.getIndicador());
		bloco1.setIndicador(10, 39);
		tent1.setCampo(bloco1.getIndicador());
		fish1.setIndicador(13, 19);
		tent1.setCampo(fish1.getIndicador());
		ship1.setIndicador(10 , 25);
		tent1.setCampo(ship1.getIndicador());

		bloco1.apagador();
		fish1.apagador();
		ship1.apagador();
	}

else if(decide1 == 15){
	Celula prim1;
	for(int i = 0; i < 30 ; i++){
		for(int j = 0; j < 66 ; j++){
			if (i%2 != 0 && j%2 == 0) {
				prim1.setIndicador(i , j);
				tent1.setCampo(prim1.getIndicador());
			}
			else if (i%2 == 0 && j%2 != 0) {
				prim1.setIndicador(i , j);
				tent1.setCampo(prim1.getIndicador());
			}
		}
	}
	prim1.apagador();
}

else{
	Celula prim1;
	for(int i = 0; i < 30 ; i++){
		for(int j = 0; j < 66 ; j++){
			if (i%2 != 0 && j%2 == 0) {
				prim1.setIndicador(i , j);
				tent1.setCampo(prim1.getIndicador());
			}
			else if (i%2 == 0 && j%2 != 0) {
				prim1.setIndicador(i , j);
				tent1.setCampo(prim1.getIndicador());
			}
		}
	}
	prim1.apagador();
	cout << "\nJogo da Vida\n" << endl;
	tent1.Desenhar();
	cout << "\n\nERROR! #02" << endl;
	cout << "Opção Inválida\n\n\n\n\n\n" << endl;
	decide4 = 0;
}

	if(decide4 != 0){
		cout << "\nJogo da Vida\n" << endl;
		tent1.Desenhar();
		cout << "\n\nCom Quantas gerações deseja executar o jogo?" << endl;
		cout << "\n\n\n\n\n\n" << endl;
		cin >> decide1;
		tent1.Executor(decide1);
	}

}

else{
	Celula prim1;
	for(int i = 0; i < 30 ; i++){
		for(int j = 0; j < 66 ; j++){
			if (i%2 != 0 && j%2 == 0) {
				prim1.setIndicador(i , j);
				tent1.setCampo(prim1.getIndicador());
			}
			else if (i%2 == 0 && j%2 != 0) {
				prim1.setIndicador(i , j);
				tent1.setCampo(prim1.getIndicador());
			}
		}
	}
	prim1.apagador();

	cout << "\nJogo da Vida\n" << endl;
	tent1.Desenhar();
	cout << "\n\nERROR! #01" << endl;
	cout << "Opção Inválida\n\n\n\n\n\n" << endl;
}


return 0;
}
