#include "matriz.hpp"

#include <unistd.h>
#include <iostream>
#include <stdlib.h>

using namespace std;

Matriz::Matriz(){

	for(int a = 0; a < 100; a++){
	    for(int b = 0; b < 100; b++){
	    	campo[a][b] = false;
	    }
	}
}

Matriz::~Matriz(){

}


bool Matriz::getCampo(int i, int j){
        return campo[i][j];
        }

void Matriz::setCampo(bool** indicador){
				for(int k = 0; k < 100; k++){
					for(int l = 0; l < 100; l++){
						if(indicador[k][l] == true){
							campo[k][l] = true;
						}
					}
				}
}

void Matriz::Desenhar(){

		for(int i = 0; i < 30 ; i++){
				for(int j = 0; j < 66 ; j++){
						if(campo[i][j] == true){
								canvas[i][j] = '0';
						}
						else if(campo[i][j] == false){
								canvas[i][j] = '-';
						}
		cout << canvas[i][j] << " ";
				}
		cout << endl;
		}
}

void Matriz::AplicaRegra(){

        for(int i = 0; i < 30 ; i++){
            for(int j = 0; j < 67 ; j++){

							int cont2 = 0;

							if(i == 0 && j == 0 ){ 				//Começo da regra da borda - Primeira Posição
								if(campo[i][j+1] == true){
									cont2++;
								}
								if(campo[i+1][j+1] == true){
									cont2++;
								}
								if(campo[i+1][j] == true){
									cont2++;
								}
							}

							else if( i == 29 && j == 66 ){ // Ultima Posição
								if(campo[i-1][j-1] == true){
									cont2++;
								}
								if(campo[i-1][j] == true){
									cont2++;
								}
								if(campo[i][j-1] == true){
									cont2++;
								}
							}

							else if( i == 0 && j == 66 ){ // Canto superior Direito
								if(campo[i+1][j] == true){
									cont2++;
								}
								if(campo[i+1][j-1] == true){
								cont2++;
								}
								if(campo[i][j-1] == true){
									cont2++;
								}
							}

							else if( i == 29 && j == 0 ){ //Canto inferior Esquerdo
								if(campo[i-1][j] == true){
									cont2++;
								}
								if(campo[i-1][j+1] == true){
									cont2++;
								}
								if(campo[i][j+1] == true){
									cont2++;
								}
							}

							else if( i == 0 && j < 66 && j > 0 ){ //Borda Superior
								if(campo[i][j+1] == true){
									cont2++;
								}
								if(campo[i+1][j+1] == true){
									cont2++;
								}
								if(campo[i+1][j] == true){
									cont2++;
								}
								if(campo[i+1][j-1] == true){
								cont2++;
								}
								if(campo[i][j-1] == true){
									cont2++;
								}
							}

							else if( i == 29 && j < 66 && j > 0 ){ //Borda Inferior
								if(campo[i-1][j-1] == true){
									cont2++;
								}
								if(campo[i-1][j] == true){
									cont2++;
								}
								if(campo[i-1][j+1] == true){
									cont2++;
								}
								if(campo[i][j+1] == true){
									cont2++;
								}
								if(campo[i][j-1] == true){
									cont2++;
								}
							}

							else if( i < 29 && i > 0 && j == 0 ){ //Borda Lateral Esquerda
								if(campo[i-1][j] == true){
									cont2++;
								}
								if(campo[i-1][j+1] == true){
									cont2++;
								}
								if(campo[i][j+1] == true){
									cont2++;
								}
								if(campo[i+1][j+1] == true){
									cont2++;
								}
								if(campo[i+1][j] == true){
									cont2++;
								}
							}

							else if( i < 29 && i > 0 && j == 66){ //Borda Lateral Direita
								if(campo[i-1][j-1] == true){
									cont2++;
								}
								if(campo[i+1][j-1] == true){
								cont2++;
								}
								if(campo[i][j-1] == true){
									cont2++;
								}
								if(campo[i+1][j] == true){
									cont2++;
								}
								if(campo[i-1][j] == true){
									cont2++;
								}
							}                            //Fim das Regras da Borda

							else {
									if(campo[i-1][j-1] == true){
										cont2++;
									}
									if(campo[i-1][j] == true){
										cont2++;
									}
									if(campo[i-1][j+1] == true){
										cont2++;
									}
	  							if(campo[i][j+1] == true){
										cont2++;
									}
									if(campo[i+1][j+1] == true){
										cont2++;
									}
									if(campo[i+1][j] == true){
										cont2++;
									}
									if(campo[i+1][j-1] == true){
	  							cont2++;
									}
									if(campo[i][j-1] == true){
										cont2++;
									}
								}

							if(campo[i][j] == true && cont2 < 2){
									trans[i][j] = false;
							}

							else if(campo[i][j] == true && cont2 > 3){
									trans[i][j] = false;
							}

							else if(campo[i][j] == false && cont2 == 3){
									trans[i][j] = true;
							}

							else if(campo[i][j] == true && cont2 == 3){
									trans[i][j] = true;
							}

							else if(campo[i][j] == true && cont2 == 2){
									trans[i][j] = true;
							}

							else{
									trans[i][j] = false;
							}


        }
    }

		for(int i = 0; i < 30; i++){
				for(int j = 0; j < 66 ; j++){
						campo[i][j] = trans[i][j];
				}
		}

}

void Matriz::Executor(int geracoes){

		for(int cal = 0; cal < geracoes; cal++){
			cout << "\nJogo da Vida\n" << endl;
	 		Desenhar();
	 		AplicaRegra();
			cout << "\n\nEm execução\n" << endl;
			cout << "Geração ";
			cout << cal+1;
			cout << "\n\n\n\n\n" << endl;
			usleep(80000);

		//clear;
	}
}
