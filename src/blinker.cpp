#include<iostream>
#include "blinker.hpp"

using namespace std;

Blinker::Blinker(){
	name3 = "Blinker";
	setTipo(name3);
}

Blinker::~Blinker(){

}

void Blinker::setIndicador(int linha, int coluna){
		this->indicador[linha][coluna] = true;
		this->indicador[linha-1][coluna] = true;
		this->indicador[linha+1][coluna] = true;
}

bool** Blinker::getIndicador(){
		return indicador;
}
